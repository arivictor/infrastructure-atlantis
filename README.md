**Build**
```
docker build -t atlantis-custom .
```

**Server-side Config**
```
# repo.yaml
```

**Repo-based Config**
```
# atlantis.yaml
```

**Run**
```
docker run atlantis-custom 8080:4141
```