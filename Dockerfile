
# Download Terragrunt
FROM debian as builder

# ARG TERRAGRUNT_VERSION

WORKDIR /builder/terragrunt
RUN apt-get update
RUN apt-get -y install unzip wget curl ca-certificates

RUN wget -q https://github.com/gruntwork-io/terragrunt/releases/download/v0.31.1/terragrunt_linux_amd64

# Create custom Atlantis image with Terragrunt
FROM runatlantis/atlantis:v0.17.2

ENV PATH=/builder/terragrunt/:$PATH

COPY --from=builder /builder/terragrunt/terragrunt_linux_amd64 ./usr/local/bin/terragrunt

RUN chmod +x ./usr/local/bin/terragrunt
